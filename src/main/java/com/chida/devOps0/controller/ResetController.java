package com.chida.devOps0.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chida.devOps0.model.ResetPassword;
import com.chida.devOps0.service.UserService;

@Controller
public class ResetController {
	@Autowired
	UserService userService;

	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("reset");
		mav.addObject("reset", new ResetPassword());
		return mav;
	}

	@RequestMapping(value = "/resetProcess", method = RequestMethod.POST)
	public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("reset") ResetPassword resetPassword) {
		ModelAndView mav = null;
		mav = new ModelAndView("welcome");
		mav.addObject("firstname", resetPassword.getUsername());
		return mav;
	}
}
